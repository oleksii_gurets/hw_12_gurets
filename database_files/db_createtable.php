<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/database_files/db_connect.php";
try{
	$sql = 'CREATE TABLE goods (
		id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
		title VARCHAR(255),
		price FLOAT,
		description VARCHAR(255),
		type VARCHAR(255)
		) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;';
	$connect->exec($sql);
}catch(Exception $errorCreation){
	echo 'Error creating TABLE: goods<br>';
	echo $errorCreation->getMessage();
	echo '<br><a href="/">На главную</a>';
	die();
}
echo 'TABLE goods created succesfully!<br>';
echo '<a href="/">На главную</a>';
die();
?>