<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/database_files/db_connect.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/classes/device_class.php";
$testData = [
    [
        'title' => 'Casio G-Shock G-2900F',
        'price' => 870.8,
        'description' => 'Мужские часы Casio,(тестовые данные для заполнения)',
        'type' => 'watch'
    ],
    [
        'title' => 'Xiaomi Redmi Note 10 Pro',
        'price' => 320.5,
        'description' => '6/64 Glacier Blue,(тестовые данные для заполнения)',
        'type' => 'phone'
    ],
    [
        'title' => 'HP ProBook 450 G7',
        'price' => 1117.2,
        'description' => '(6YY21AV_V13) Pike Silver,(тестовые данные для заполнения)',
        'type' => 'laptop'
    ]
];
/*echo '<pre>';
print_r($testData);*/
try{
    foreach($testData as $testItem){
        $tGoodsItem = new Device($testItem['title'], $testItem['price'], $testItem['description'], $testItem['type']);
        $tGoodsItem->saveItem($connect);
    }
}catch(Exception $errorTest){
    die("Error while adding Test data.<br>".$errorTest->getMessage());
}
echo "DATABASE Table goods was filled with 3 items<br>";
echo '<a href="/">На главную</a>';
?>