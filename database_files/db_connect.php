<?php
try{
	$connect = new PDO('mysql:host=localhost;dbname=goods', 'root', 'root');
	$connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$connect->exec('SET NAMES "utf8"');
}catch(Exception $errorAccess){
	echo 'No database connection!';
	echo $errorAccess->getMessage();
	die();
}
?>