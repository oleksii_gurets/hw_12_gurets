<?php
class Device{
	protected $id = 0;
	protected $title = "";
	protected $price = 0.0;
	protected $description = "";
	protected $type = "";

	public function __construct($title, $price, $description, $type, $id = null){
		$this->id = $id;
		$this->title = $title;
		$this->price = $price;
		$this->description = $description;
		$this->type = $type;
	}

	public function saveItem(PDO $connect){
		try{
			$sql ="INSERT INTO goods SET
			title=:title,
			price=:price,
			description=:description,
			type=:type
			";
			$state = $connect->prepare($sql);
			$state->bindValue(':title', $this->title);
			$state->bindValue(':price', $this->price);
			$state->bindValue(':description', $this->description);
			$state->bindValue(':type', $this->type);
			$state->execute();
		}catch(Exception $errorSave){
			die('Error creating new goods item!<br>' . $errorSave->getMessage());
		}
	}

    static public function allInfo(PDO $connect){
        try{
            $sql = "SELECT * FROM goods";
            $gottenObject = $connect->query($sql);
            $goodsArray = $gottenObject->fetchAll();
            $goodsObjects = [];
            foreach($goodsArray as $goodsItem){
                $goodsObjects[] = new Device($goodsItem['title'], $goodsItem['price'], $goodsItem['description'], $goodsItem['type'], $goodsItem['id']);
            }
            return $goodsObjects;
        }catch(Exception $errorGoods){
            die('Error getting goods!<br>' . $errorGoods->getMessage());
        }
    }

    public function display($template){
        include $_SERVER['DOCUMENT_ROOT'].'/templates/'.$template.'.php';
    }

	static public function chooseItem($id, $connect){
		try {
			$sql = "SELECT * FROM goods WHERE id = :id;";
			$state = $connect->prepare($sql);
			$state->bindValue(':id', $id);
			$state->execute();
			$goodsArray = $state->fetchAll();
			return new self($goodsArray[0]['title'], $goodsArray[0]['price'], $goodsArray[0]['description'], $goodsArray[0]['type'], $goodsArray[0]['id']);
		} catch (Exception $errorChoose){
			die('Error creating current goods item!<br>' . $errorChoose->getMessage());
		}
	}

	public function updateItem(PDO $connect){
        try{
            $sql = "UPDATE goods SET
                title=:title,
                price=:price,
                description=:description,
				type=:type
             WHERE id = :id;";
            $state = $connect->prepare($sql);
            $state->bindValue(':title', $this->title);
            $state->bindValue(':price', $this->price);
            $state->bindValue(':description', $this->description);
			$state->bindValue(':type', $this->type);
            $state->bindValue(':id', $this->id);
            $state->execute();

        }catch(Exception $errorUpdate){
            die('Error updating goods item!<br>'. $errorUpdate->getMessage());
        }
    }

	static public function deleteItem($id, $connect){
        try{
            $sql = "DELETE FROM goods WHERE id=:id";
            $state = $connect->prepare($sql);
            $state->bindValue(':id', $id);
            $state->execute();
        }catch(Exception $errorDelete){
            die('Error deleting note<br>'.$errorDelete->getMessage());
        }
    }
}
?>