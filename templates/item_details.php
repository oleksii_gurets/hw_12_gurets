<div class="container">
    <div class="row">
        <h2>Наименование товара: <?= $this->title?></h2>
        <h2>Цена товара: <?= $this->price . " usd"?></h2>
        <p class="fs-3 fst-italic">Описание товара: <?= $this->description?></p>
        <p class="fs-3 text-uppercase">Тип товара: <?= $this->type?></p>
        <a href="/" class="pagination" class="page-link"><<<</a>
    </div>  
</div>
