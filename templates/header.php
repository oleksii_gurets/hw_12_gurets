<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Домашнее задание №12 Гурца Алексея</title>
    <meta name="description" content="">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

</head>

<body>
    <div class="container-fluid">
            <header class="p-3 m-3 text-warning" style="background-color: #4b6477;">
                <div class="text-center">
                    <h1>Домашнее задание №12</h1>
                    <h2>Параметризированные запросы и статические методы</h2>
                </div>
            </header>
    </div>