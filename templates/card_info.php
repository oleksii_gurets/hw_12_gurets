<div class="col-sm-4">
    <div class="card text-white bg-secondary mb-3">
        <div class="card-header">
            Карточка товара
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item"><strong>Наименование товара: </strong><?=$this->title?></li>
            <li class="list-group-item"><strong>Цена товара: </strong><?=$this->price . " usd"?></li>
            <li class="list-group-item">
                <div class="buttons">
                    <a href="/crud_files/show_item.php?id=<?=$this->id?>" class="btn btn-primary">Детально</a>
                    <a href="/crud_files/edit_item.php?id=<?=$this->id?>" class="btn btn-warning">Редактировать</a>    
                    <form style="display:inline-block" method="post" action="/crud_files/delete_item.php">
                        <input type="hidden" name="id" value="<?=$this->id?>">
                        <button class="btn btn-danger">Удалить товар</button>
                    </form>   
                </div>  
            </li>
        </ul>
    </div>
</div>