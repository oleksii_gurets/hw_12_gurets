<?php include_once $_SERVER['DOCUMENT_ROOT']."/templates/header.php";?>

<div class="container">
	<div class="row">
		<h1>Редактировать выбранный товар</h1>
		<form action="/crud_files/update_item.php" method="POST" class="w-50 p-3" style="background-color: tan;">
			<input type="hidden" name="id" value="<?=$this->id?>">
			<div class="mb-3">
				<label for="title" class="form-label">Название товара:</label>
				<input type="text" class="form-control" value="<?=$this->title?>" name="title" id="title">
			</div>
			<div class="mb-3">
				<label for="price" class="form-label">Цена товара:</label>
				<input type="text" class="form-control" value="<?=$this->price?>" name="price" id="price">
			</div>
			<div class="mb-3">
				<label for="description" class="form-label">Описание товара:</label>
				<textarea name="description" class="form-control" id="description"><?=$this->description?></textarea>
			</div>
			<div class="mb-3">
				<select name="type">
					<option selected value="choose" disabled><?=$this->type?></option>
					<option value="phone">phone</option>
					<option value="laptop">laptop</option>
					<option value="watch">watch</option>
				</select>
			</div>
			<button type="submit" class="btn btn-secondary">Обновить товар</button>
		</form>
	</div>
</div>
	    
<?php include_once $_SERVER['DOCUMENT_ROOT']."/templates/footer.php";?>