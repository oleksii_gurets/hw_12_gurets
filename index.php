<?php
/*header('Content-type: text/html; charset=utf-8');*/
require_once $_SERVER['DOCUMENT_ROOT'] . "/database_files/db_connect.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/classes/device_class.php";

$gottenGoods = Device::allInfo($connect);
//echo '<pre>';
//print_r($gottenGoods);
?>
<?php include_once $_SERVER['DOCUMENT_ROOT']."/templates/header.php";?>

<div class="container">
    <div class="row">
        <?php if(!empty($_GET['notification'])):?>
            <?php if($_GET['notification'] == 'entry_saved'):?>
                <div class="alert alert-primary" role="alert">
                    Товар успешно добавлен!
                </div>
            <?php elseif($_GET['notification'] == 'entry_updated'):?>
                <div class="alert alert-primary" role="alert">
                    Товар успешно отредактирован!
                </div>
            <?php endif;?>
        <?php endif;?>
    </div>
</div>
<ul class="nav justify-content-center">
    <li class="nav-item">
        <a class="nav-link" href="/crud_files/create_page.php">Создать новый товар</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="/database_files/db_createtable.php">Создать таблицу базы данных</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="/database_files/db_seeder.php">Заполнить таблицу тестовыми данными</a>
    </li>
</ul>
<div class="container">
    <div class="row">
            <?php foreach($gottenGoods as $goodsItem):?>
                <?php $goodsItem->display('card_info')?>
            <?php endforeach;?>
    </div>
</div>

<?php include_once $_SERVER['DOCUMENT_ROOT']."/templates/footer.php";?>