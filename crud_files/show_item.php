<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/database_files/db_connect.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/classes/device_class.php";

try{

    if(empty($_GET['id'])){
        header('Location:/');
    }
    $id = (int)$_GET['id'];
    $currentItem = Device::chooseItem($id, $connect);
    
}catch(Exception $errorItem){
    die('Error getting chosen goods item!<br>'.$errorItem->getMessage());
}
?>
<?php include_once $_SERVER['DOCUMENT_ROOT']."/templates/header.php";?>

<div class="container">
    <?php $currentItem->display('item_details')?>  
</div>

<?php include_once $_SERVER['DOCUMENT_ROOT']."/templates/footer.php";?>