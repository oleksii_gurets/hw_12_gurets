<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/database_files/db_connect.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/classes/device_class.php";

if(empty($_POST['id'])){
    header('Location:/');
}
$id = $_POST['id'];
Device::deleteItem($id, $connect);
header("Location:/");
?>