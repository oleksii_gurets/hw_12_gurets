<?php
if(empty($_POST['title']) || empty($_POST['price']) || empty($_POST['description'] || empty($_POST['type']) || empty($_POST['id']))){
    header('Location:/');
}
require_once $_SERVER['DOCUMENT_ROOT'] . "/database_files/db_connect.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/classes/device_class.php";

$id = $_POST['id'];
$title = htmlspecialchars($_POST['title'], ENT_QUOTES, 'UTF-8');
$price = htmlspecialchars($_POST['price'], ENT_QUOTES, 'UTF-8');
$description = htmlspecialchars($_POST['description'], ENT_QUOTES, 'UTF-8');
$type = htmlspecialchars($_POST['type'], ENT_QUOTES, 'UTF-8');

$currentItem = Device::chooseItem($id, $connect);
$updatedItem = new Device($title, $price, $description, $type, $id);

/*echo '<pre>';
print_r($currentItem);
print_r($updatedItem);*/

$updatedItem->updateItem($connect);
header('Location:/?notification=entry_updated');
?>