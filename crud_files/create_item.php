<?php
if(empty($_POST['title']) || empty($_POST['price']) || empty($_POST['description']) || empty($_POST['type'])){
	header('Location:/crud_files/create_page.php');
}
require_once $_SERVER['DOCUMENT_ROOT'] . "/database_files/db_connect.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/classes/device_class.php";

$title = htmlspecialchars($_POST['title'], ENT_QUOTES, 'UTF-8');
$price = htmlspecialchars($_POST['price'], ENT_QUOTES, 'UTF-8');
$description = htmlspecialchars($_POST['description'], ENT_QUOTES, 'UTF-8');
$type = htmlspecialchars($_POST['type'], ENT_QUOTES, 'UTF-8');

$goodsItem = new Device($title, $price, $description, $type);
$goodsItem->saveItem($connect);
header('Location:/?notification=entry_saved'); 
?>