<?php include_once $_SERVER['DOCUMENT_ROOT']."/templates/header.php";?>

<div class="container">
	<div class="row">
		<h1>Создать новый товар</h1>
		<form action="/crud_files/create_item.php" method="POST" class="w-50 p-3" style="background-color: tan;">
			<div class="mb-3">
				<label for="title" class="form-label">Название товара:</label>
				<input type="text" class="form-control" name="title" id="title">
			</div>
			<div class="mb-3">
				<label for="price" class="form-label">Цена товара:</label>
				<input type="text" class="form-control" name="price" id="price">
			</div>
			<div class="mb-3">
				<label for="description" class="form-label">Описание товара:</label>
				<textarea name="description" class="form-control" id="description"></textarea>
			</div>
			<div class="mb-3">
				<select name="type">
					<option selected value="choose" disabled>Выберите тип товара:</option>
					<option value="phone">phone</option>
					<option value="laptop">laptop</option>
					<option value="watch">watch</option>
				</select>
			</div>
			<button type="submit" class="btn btn-secondary">Создать товар</button>
		</form>
	</div>
</div>    

<?php include_once $_SERVER['DOCUMENT_ROOT']."/templates/footer.php";?>